import React from 'react';
import Navbar from '../componentes/Navbar';
import SidebarMenu from '../componentes/SidebarMenu';
import ContentHeader from '../componentes/ContentHeader';
import { Link } from 'react-router-dom';

const DashBoard = () => {
    return (
        <>
            <Navbar></Navbar>
            <SidebarMenu></SidebarMenu>

            <main id="main" className="main">
                <ContentHeader
                    titulo={"TuDoctorOnline"}
                    breadCrumb1={"Agendamiento de servicios médicos."}
                    breadCrumb2={""}
                    breadCrumb3={""}
                    ruta={"/menu-principal"}
                />

                <section className="section dashboard">
                    <div className="row">
                        <div className="col-lg-12">
                            <div className="row">

                                <div className="col-xxl-3 col-md-6">
                                    <div className="card info-card sales-card">
                                        <div className="card-body">                                          
                                            <h5 className="card-title">ROLES <Link to={"/roles-admin"}><span>| Listar Roles</span></Link></h5>
                                            <div className="d-flex align-items-center">
                                                <div className="card-icon rounded-circle d-flex align-items-center justify-content-center">
                                                    <i className="bi bi-person-fill" />
                                                </div>                                             
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div className="col-xxl-3 col-md-6">
                                    <div className="card info-card sales-card">
                                        <div className="card-body">
                                            <h5 className="card-title">ESPECIALIDADES <Link to={"/especialidades-admin"}><span>| Listar Especialidades</span></Link></h5>
                                            <div className="d-flex align-items-center">
                                                <div className="card-icon rounded-circle d-flex align-items-center justify-content-center">
                                                    <i className="bi bi-clipboard-plus" />
                                                </div>                                             
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div className="col-xxl-3 col-md-6">
                                    <div className="card info-card sales-card">
                                        <div className="card-body">
                                            <h5 className="card-title">USUARIOS <Link to={"/usuarios-admin"}><span>| Listar Usuarios</span></Link></h5>
                                            <div className="d-flex align-items-center">
                                                <div className="card-icon rounded-circle d-flex align-items-center justify-content-center">
                                                    <i className="bi bi-people-fill" />
                                                </div>                                             
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div className="col-xxl-3 col-md-6">
                                    <div className="card info-card sales-card">
                                        <div className="card-body">
                                            <h5 className="card-title">AGENDAR CITAS <Link to={"/agenda-citas-admin"}><span>| Listar Citas</span></Link></h5>
                                            <div className="d-flex align-items-center">
                                                <div className="card-icon rounded-circle d-flex align-items-center justify-content-center">
                                                    <i className="bi bi-calendar3" />
                                                </div>                                             
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                </section>
            </main>
        </>
    );
}

export default DashBoard;